"""Business usecases and pipelines
"""
from datetime import datetime
import json
import logging
from django.conf import settings
from openai import OpenAI
from openai.types.chat import ChatCompletion
from ingestion.models import AbstractEmailRaw

from clustering.models import GroupMessage

logger = logging.getLogger()


class AbstractOpenAIUseCase:
    """Abstract class to run open AI use cases
    """
    api_key = settings.OPENAI_API_KEY
    model= "gpt-3.5-turbo"

    def __init__(self) -> None:
        self.open_ai_client = OpenAI(api_key=self.api_key)

    def get_sys_template(self) -> dict:
        """Separates the template for the role of the system
        """
        return [
            {
                'role': 'system',
                'content': (
                    'you are a labeler and labels categorizes messages as one of the following categories:'
                    'Anniversary,App Download,Back in Stock,Birthday,BOPO,Bounceback,Browse Abandon,Cart '
                    'Abandon,Cartful,Category Abandon,Category Recommendation,Checkout Abandon,Coupon,'
                    'Credit Reminder,Cross Promote,Drip Series,Favorites,Giveaway,Identity+ Activation,'
                    'Internal,Lapsed Reminder,Low Inventory,Loyalty,Miscellaneous,Opt-in Coupon,Post Purchase,'
                    'Price Drop,Promo,Quiz,Reactivation,Recommendations,Reengagement,Retention,Rewards,'
                    'Sageflo Archiver,Save for Later,Search Abandon,Store/Event,Transactional,VIP Lapsing,'
                    'Welcome,Weather Series,Winback,Wishlist.'
                )
            }, {
                'role': 'user',
                'content': (
                    """
                    return only a json document with the analysis on the messages below. Do not 
                    responde anything else besides the json.
                    \n
                    [{"id": 1, "message": "Hurry! Limited time left for Halloween favorites ðŸŽƒðŸ’€"},
                    {"id": 2, "message": "Switch things up with Switch Mats"},
                    {"id": 3, "message": "Solar accents to brighten gardens for fall & winter ðŸŒŸ"},
                    {"id": 4, "message": "20% Off Decorative Accents EXTENDED"}]
                    """
                )
            }, {
                'role': 'assistant',
                'content': (
                    """
                    [
                        {"id": 1, "category": "Promo"},
                        {"id": 2, "category": "Promo"},
                        {"id": 3, "category": "Promo"},
                        {"id": 4, "category": "Promo"}
                    ]
                    """
                )
            }
        ]

    def query_gpt(self, msgs: list[AbstractEmailRaw]) -> ChatCompletion:
        """Run the GPT query
        """
        items = []
        for msg in msgs:
            tags = msg.tags or ''
            message_name = msg.message_name or ''
            podium_orchestration_name = msg.podium_orchestration_name or ''

            items.append({
                "id": msg.id,
                "message": (
                    msg.subject.strip() +
                    tags +
                    message_name or '' +
                    podium_orchestration_name or ''
                ),
            })

        item_str = json.dumps(items)
        template = self.get_sys_template() + [
            {
               'role': 'user',
                'content': (
                    """
                    return only a json document with the analysis on the messages below. Do not 
                    responde anything else besides the json.
                    \n
                    """
                    + item_str
                ),
            }
        ]

        return self.open_ai_client.chat.completions.create(
            model=self.model,
            messages=template,
            stream=False,
        )

    def to_dict(self, response: ChatCompletion) -> list:
        """Converts gpt response to dict"""
        content = response.choices[0].message.content
        content_list = []
        try:
            content_list = json.loads(content)
        except json.JSONDecodeError:
            logger.error(f'Unable to decode json {content}')

        return content_list
    
    def get_by_id(self, msgs: list[AbstractEmailRaw]) -> dict[int, ChatCompletion]:
        """Prepare data with ids as the key
        """
        return self.to_dict(self.query_gpt(msgs))

    def run(self, msgs: AbstractEmailRaw) -> dict[int, ChatCompletion]:
        """Runs use case
        """
        ...


class ClusteringGPTAnalysisUseCase(AbstractOpenAIUseCase):
    """Runs a sentiment analysis
    """

    def run(self, msgs: AbstractEmailRaw) -> dict[int, dict]:
        """Runs use case
        """
        return self.get_by_id(msgs)


class TimeAnalysisUseCase:
    """Checks whether the time is early in the month, mid month, or end of the month
    """

    def convert_date(self, date_str: str) -> datetime:
        """try different templates to convert a date"""
        try:
            converted_dt = datetime.strptime(date_str, "%m/%d/%Y")
        except ValueError:
            converted_dt = datetime.strptime(date_str, "%m/%d/%y")

        return converted_dt

    def run(self, msgs: AbstractEmailRaw) -> dict[int, str]:
        """Runs use case
        """
        result = {}
        for msg in msgs:
            converted_dt = self.convert_date(msg.sent_date)
            if converted_dt.day <= 10:
                dt_type = 'early-month'
            elif converted_dt.day <= 20:
                dt_type = 'mid-month'
            else:
                dt_type = 'late-month'

            result[msg.id] = dt_type

        return result


class ClusteringUseCase:
    """Runs all cluster analysis
    """

    def __init__(self, email_raw_type: str):
        self.time_analysis = {}
        self.email_raw_type = email_raw_type

    def mutate_map_item(self, content: tuple[int, dict]) -> tuple:
        """Sets the time analysis to map"""
        content[1]['time_analysis'] = self.time_analysis[content[0]]
        return (content[0], content[1])

    def save(self, data: list[dict]):
        """Save a group"""
        groups = []
        for content in data:
            groups.append(GroupMessage(
                email_raw_id=content.get('id'),
                email_raw_type=self.email_raw_type,
                message_type=content.get('category'),
            ))

        GroupMessage.objects.bulk_create(groups)

    def run(self, msgs: AbstractEmailRaw):
        """Finding clusters
        """
        self.time_analysis = TimeAnalysisUseCase().run(msgs)

        analysis = ClusteringGPTAnalysisUseCase().run(msgs)
        self.save(analysis)
        return analysis


def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in range(0, len(lst), n):
        yield lst[i:i + n]


def run_usecases(brand: str, raw_type: str, max_length: int = 300):
    from ingestion.models import AutomationsEmailRaw, BatchEmailRaw

    if raw_type == 'ingestion_automationsemailraw':
        msgs = AutomationsEmailRaw.objects.filter(brand=brand)[:max_length]
    else:
        msgs = BatchEmailRaw.objects.filter(brand=brand)[:max_length]

    for items in chunks(msgs, 30):
        uc = ClusteringUseCase(raw_type)
        yield uc.run(items)
