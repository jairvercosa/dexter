"""Models for clustering
"""
from django.db import models


class GroupMessage(models.Model):
    """Clusters representation
    """

    email_raw_id = models.IntegerField()
    email_raw_type = models.CharField(max_length=255)

    sentiment = models.CharField(max_length=255, null=True, blank=True)
    message_type = models.CharField(max_length=255, null=True, blank=True)
    target = models.CharField(max_length=255, null=True, blank=True)
    time_analysis = models.CharField(max_length=255, null=True, blank=True)
