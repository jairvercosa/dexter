"""Contain representatiosn for automations_email_raw and batch_email_raw
"""
from django.forms.models import model_to_dict
from django.db import models


class AbstractEmailRaw(models.Model):
    """The interface for EmailRaw
    """
    brand = models.CharField(max_length=255, null=True, blank=True)
    message_id = models.CharField(max_length=255, null=True, blank=True)
    message_name = models.TextField(null=True, blank=True)
    message_key = models.CharField(max_length=255, null=True, blank=True)
    message_type = models.CharField(max_length=255, null=True, blank=True)
    podium_orchestration_name = models.CharField(max_length=255, null=True, blank=True)
    subject = models.TextField(null=True, blank=True)
    tags = models.CharField(max_length=255, null=True, blank=True)

    def to_dict(self) -> dict:
        """Converts object to dictionaty"""
        return model_to_dict(self)

    class Meta:
        """Django Model configs
        """
        abstract = True


class BatchEmailRaw(AbstractEmailRaw):
    """Represents the batch_email_raw data
    """
    sent_date = models.CharField(max_length=255, null=True, blank=True)
    last_sent_date = models.CharField(max_length=255, null=True, blank=True)
    sent_total = models.CharField(max_length=255, null=True, blank=True)
    bounced_total = models.CharField(max_length=255, null=True, blank=True)
    bounce_rate = models.CharField(max_length=255, null=True, blank=True)
    bounced_hard = models.CharField(max_length=255, null=True, blank=True)
    bounced_soft = models.CharField(max_length=255, null=True, blank=True)
    delivered_total = models.CharField(max_length=255, null=True, blank=True)
    delivered_rate = models.CharField(max_length=255, null=True, blank=True)
    opens_total = models.CharField(max_length=255, null=True, blank=True)
    opens_unique = models.CharField(max_length=255, null=True, blank=True)
    open_rate = models.CharField(max_length=255, null=True, blank=True)
    opens_total_adjusted = models.CharField(max_length=255, null=True, blank=True)
    opens_unique_adjusted = models.CharField(max_length=255, null=True, blank=True)
    open_rate_adjusted = models.CharField(max_length=255, null=True, blank=True)
    click_total = models.CharField(max_length=255, null=True, blank=True)
    clicks_unique = models.CharField(max_length=255, null=True, blank=True)
    ctor = models.CharField(max_length=255, null=True, blank=True)
    click_rate = models.CharField(max_length=255, null=True, blank=True)
    complaints = models.CharField(max_length=255, null=True, blank=True)
    complaint_rate = models.CharField(max_length=255, null=True, blank=True)
    opt_outs = models.CharField(max_length=255, null=True, blank=True)
    opt_out_rate = models.CharField(max_length=255, null=True, blank=True)
    revenue = models.CharField(max_length=255, null=True, blank=True)
    total_purchases = models.CharField(max_length=255, null=True, blank=True)
    aov = models.CharField(max_length=255, null=True, blank=True)
    response_rate = models.CharField(max_length=255, null=True, blank=True)
    rp = models.CharField(max_length=255, null=True, blank=True)


class AutomationsEmailRaw(AbstractEmailRaw):
    """Represents the batch_email_raw data
    """
    automation_category_tag = models.CharField(max_length=255, null=True, blank=True)
    automation_touch_tag = models.CharField(max_length=255, null=True, blank=True)
    sent_date = models.CharField(max_length=255, null=True, blank=True)
    last_sent_date = models.CharField(max_length=255, null=True, blank=True)
    sent_total = models.CharField(max_length=255, null=True, blank=True)
    bounced_total = models.CharField(max_length=255, null=True, blank=True)
    bounce_rate = models.CharField(max_length=255, null=True, blank=True)
    bounced_hard = models.CharField(max_length=255, null=True, blank=True)
    bounced_soft = models.CharField(max_length=255, null=True, blank=True)
    delivered_total = models.CharField(max_length=255, null=True, blank=True)
    delivered_rate = models.CharField(max_length=255, null=True, blank=True)
    opens_total = models.CharField(max_length=255, null=True, blank=True)
    opens_unique = models.CharField(max_length=255, null=True, blank=True)
    open_rate = models.CharField(max_length=255, null=True, blank=True)
    opens_total_adjusted = models.CharField(max_length=255, null=True, blank=True)
    opens_unique_adjusted = models.CharField(max_length=255, null=True, blank=True)
    open_rate_adjusted = models.CharField(max_length=255, null=True, blank=True)
    click_total = models.CharField(max_length=255, null=True, blank=True)
    clicks_unique = models.CharField(max_length=255, null=True, blank=True)
    ctor = models.CharField(max_length=255, null=True, blank=True)
    click_rate = models.CharField(max_length=255, null=True, blank=True)
    complaints = models.CharField(max_length=255, null=True, blank=True)
    complaint_rate = models.CharField(max_length=255, null=True, blank=True)
    opt_outs = models.CharField(max_length=255, null=True, blank=True)
    opt_out_rate = models.CharField(max_length=255, null=True, blank=True)
    revenue = models.CharField(max_length=255, null=True, blank=True)
    total_purchases = models.CharField(max_length=255, null=True, blank=True)
    aov = models.CharField(max_length=255, null=True, blank=True)
    conversion_rate = models.CharField(max_length=255, null=True, blank=True)
    rpe = models.CharField(max_length=255, null=True, blank=True)
